# web-feed-server

Simple web feed server that provides feed content management. Currently supports Atom type of web feed.


## Types of content

There are two main types of content that the server can deliver

### Static Content
This is simply a static file that is returned when requested, it can be requested as many times as wanted

### Versioned Content
Versioned content is a little more tricky. Each time a the client requests the content, the next version of the file 
will be returned on the subsequent request.


## Building

To build the project run,
`gradlew build` or `gradlew.bat build` if in windows
 
To create the application package, useful for deployment and running outside of a development environment run
`gradlew distZip`

Once the gradle task finishes you will find a zip file in the `build/distributions` folder.


## Running from the unpackaged zip

Once you have built the zip using the instructions above, unpack it to the directory of your choice. Inside you will
find the following structure.

    bin
    content
    lib
    logs

To run the application, change directory to the project root and run
`bin/web-feed-server` or `bin/web-feed-server.bat` if windows

## Managing Content

###To add a feed to the server:

1. create a `subfolder` under the `content` directory
2. add one or more files to the subfolder
3. using a HTTP client (curl, browser, programmatically, etc) insert a new content configuration to the server

   assuming the server is running locally and the subfolder path is substituted for <subfolder>

    POST http:/localhost:8080/contentconfig
    {
        "contentLocation":"<subfolder>"
    }


a response object will be returned indicating if the content added was detected as static and if not static, how many
versions of the content was detected. In this case there was only 1 file found in the subfolder so the
 content is marked as static with only one version.
    
    {
        "id": 0,
        "feedType": null,
        "contentLocation": "<subfolder>",
        "currentVersion": 1,
        "totalVersions": 1,
        "static": true,
        "inFeed": true
    }

In this second case the same POST request was made but with 2 files in the <subfolder> directory. Notice that this is not
a static feed and that there are 2 versions of the content.


    {
        "id": 0,
        "feedType": null,
        "contentLocation": "<subfolder>",
        "currentVersion": 1,
        "totalVersions": 2,
        "static": false,
        "inFeed": true
    }
    
  When the content is actually retrieved, the current version wil automatically increment. Once the last file is returned
  to the user the item will be removed from the feed and the inFeed property of the config will be changed to false.
  
  
## Accessing the feed
  
  Currently the feed is accessed like this
  
          GET http:/localhost:8080/atom
          
          <?xml version="1.0" encoding="UTF-8"?>
          <feed 
              xmlns="http://www.w3.org/2005/Atom">
              <entry>
                  <title>foo</title>
                  <link rel="alternate" href="http://localhost:8080//content/foo" />
                  <id>http://localhost:8080//content/foo</id>
                  <updated>2015-03-30T05:09:12Z</updated>
                  <summary>Updated Sun Mar 29 22:06:17 PDT 2015</summary>
              </entry>
          </feed>
           
           
 The feed will return any content that is ready to be consumed by the client. It will also contain links to retrieve 
 the actual content.
 
## Checking the current configuration status

The easiest way to get a sense of what the web feed should contain, check out all of the content configs by issuing
a get on the content config resource.

    GET http:/localhost:8080/contentconfig
 
 
## Deleting items from the feed
 
 When a content configuration item is created, an ID is returned (see above). This ID is used to remove content from the
 feed.
 
    DELETE http:/localhost:8080/contentconfig/{id}