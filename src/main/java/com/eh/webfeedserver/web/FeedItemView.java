package com.eh.webfeedserver.web;

import com.eh.webfeedserver.feed.FeedItem;
import com.eh.webfeedserver.feed.FeedItemRepository;
import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by evanharris on 3/28/15.
 */

public class FeedItemView extends AbstractAtomFeedView{


    @Autowired
    FeedItemRepository feedItemRepository;

    @Override
    protected List<Entry> buildFeedEntries(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Entry> entries = new LinkedList<>();
        List<FeedItem> feedItems = null;
        if(model.containsKey("feedType") && !StringUtils.isEmpty(model.get("feedType"))){
            feedItems = feedItemRepository.getFeedItemsByFeedType(String.valueOf(model.get("feedType")));
        }else{
            feedItems = feedItemRepository.getAllFeedItems();
        }
        if(feedItems != null){
            for(FeedItem item : feedItems){
                Entry entry = new Entry();
                entry.setTitle(item.getTitle());

                List<Link> links = new LinkedList<Link>();
                Link link = new Link();
                link.setHref(item.getUrl());
                links.add(link);

                entry.setAlternateLinks(links);

                entry.setUpdated(new Date());

                Content content = new Content();
                content.setValue(item.getSummary());
                entry.setSummary(content);

                entry.setId(item.getUrl());

                entries.add(entry);
            }
        }


        return entries;
    }
}
