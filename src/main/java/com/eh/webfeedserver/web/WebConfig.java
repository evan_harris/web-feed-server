package com.eh.webfeedserver.web;

import com.eh.webfeedserver.content.ContentRepository;
import com.eh.webfeedserver.content.ContentRepositoryImpl;
import com.eh.webfeedserver.content.config.ContentConfigRepository;
import com.eh.webfeedserver.content.config.ContentConfigRepositoryImpl;
import com.eh.webfeedserver.errorhandling.ContentException;
import com.eh.webfeedserver.feed.FeedItemRepository;
import com.eh.webfeedserver.feed.FeedItemRepositoryImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.BeanNameViewResolver;

/**
 * Created by evanharris on 3/28/15.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.eh.webfeedserver.web"})
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public BeanNameViewResolver beanNameViewResolver(){
        return new BeanNameViewResolver();
    }

    @Bean
    public FeedItemView feedItemView() {
        return new FeedItemView();
    }

    @Bean
    public FeedItemRepository feedItemRepository(){
        return new FeedItemRepositoryImpl();
    }

    @Bean
    public ContentRepository contentFileRepository() throws ContentException {
        return new ContentRepositoryImpl("content");

    }

    @Bean
    public ContentConfigRepository contentConfigRepository() throws ContentException {
        return new ContentConfigRepositoryImpl(contentFileRepository(), feedItemRepository());
    }
}