package com.eh.webfeedserver.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;

/**
 * Created by evanharris on 3/28/15.
 */
@Controller
public class AtomFeedController {

        @RequestMapping(value="/atom", method=RequestMethod.GET, produces = "text/xml; charset=utf-8")
        public ModelAndView getContent(@RequestParam(required = false) String feedType) {

            ModelAndView mav = new ModelAndView();
            mav.setViewName("feedItemView");
            mav.addObject("sampleContentList", null);
            mav.addObject("feedType", feedType);

            return mav;
        }


}
