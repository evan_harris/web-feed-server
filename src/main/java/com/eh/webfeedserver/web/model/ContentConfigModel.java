package com.eh.webfeedserver.web.model;

import com.eh.webfeedserver.content.config.ContentConfig;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by evanharris on 3/29/15.
 */
public class ContentConfigModel {



    private Integer id;

    private String feedType;

    @NotEmpty
    private String contentLocation;

    private Integer currentVersion;
    private Integer totalVersions;

    private Boolean isInFeed;

    private Boolean isStatic;

    /**
     * If urlPath is empty then the url path defaults to the content location
     */
    private String urlPath;


    public void setId(Integer id) {
        this.id = id;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public void setContentLocation(String contentLocation) {
        this.contentLocation = contentLocation;
    }

    public void setCurrentVersion(Integer currentVersion) {
        this.currentVersion = currentVersion;
    }

    public void setTotalVersions(Integer totalVersions) {
        this.totalVersions = totalVersions;
    }

    public void setIsInFeed(Boolean isInFeed) {
        this.isInFeed = isInFeed;
    }

    public Integer getId() {

        return id;
    }

    public String getFeedType() {
        return feedType;
    }

    public String getContentLocation() {
        return contentLocation;
    }

    public Integer getCurrentVersion() {
        return currentVersion;
    }

    public Integer getTotalVersions() {
        return totalVersions;
    }

    public Boolean isInFeed() {
        return isInFeed;
    }

    public void setIsStatic(Boolean isStatic) {
        this.isStatic = isStatic;
    }

    public Boolean isStatic() {

        return isStatic;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public String getUrlPath() {

        return urlPath;
    }

    public static ContentConfigModel fromContentConfig(ContentConfig contentConfig){

        ContentConfigModel contentConfigModel = new ContentConfigModel();
        contentConfigModel.setId(contentConfig.getId());
        contentConfigModel.setFeedType(contentConfig.getFeedType());
        contentConfigModel.setContentLocation(contentConfig.getContentLocation());
        contentConfigModel.setCurrentVersion(contentConfig.getCurrentContentVersion());
        contentConfigModel.setTotalVersions(contentConfig.getTotalVersions());
        contentConfigModel.setIsInFeed(contentConfig.isInFeed());
        contentConfigModel.setIsStatic(contentConfig.isStatic());
        return contentConfigModel;
    }
}
