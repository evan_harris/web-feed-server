package com.eh.webfeedserver.web.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by evanharris on 3/29/15.
 */
public class ValidationError {

    private Map<String, String> fieldErrors = new HashMap<>();





    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void addFieldError(String path, String message) {
        fieldErrors.put(path, message);
    }


}
