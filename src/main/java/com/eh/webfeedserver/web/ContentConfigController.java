package com.eh.webfeedserver.web;

import com.eh.webfeedserver.content.config.ContentConfig;
import com.eh.webfeedserver.content.config.ContentConfigRepository;
import com.eh.webfeedserver.errorhandling.ContentException;
import com.eh.webfeedserver.errorhandling.EntityNotFound;
import com.eh.webfeedserver.web.model.ContentConfigModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
@RestController
@RequestMapping(value = "/contentconfig")
public class ContentConfigController {


    @Autowired
    private ContentConfigRepository contentConfigRepository;

    @RequestMapping(method = RequestMethod.POST )
    public ContentConfigModel insert(@Valid @RequestBody ContentConfigModel contentConfigModel, HttpServletRequest request) throws ContentException, MalformedURLException {
        URL url = new URL(request.getRequestURL().toString());
        String basePath = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort();

        ContentConfig config = contentConfigRepository.insertContentConfig(contentConfigModel.getFeedType(),
                contentConfigModel.getContentLocation(), contentConfigModel.isInFeed(), basePath, contentConfigModel.getUrlPath());

        return ContentConfigModel.fromContentConfig(config);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ContentConfigModel getById(@PathVariable("id") Integer id){
        ContentConfig contentConfig = contentConfigRepository.getContentConfigById(id);
        return ContentConfigModel.fromContentConfig(contentConfig);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ContentConfigModel> getAll(){
        List<ContentConfig> contentConfigs = contentConfigRepository.getAllContentConfigs();

        List<ContentConfigModel> resultList = new LinkedList<ContentConfigModel>();
        for(ContentConfig contentConfig : contentConfigs){
            resultList.add(ContentConfigModel.fromContentConfig(contentConfig));
        }
        return resultList;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteContentConfig(@PathVariable("id") Integer id) throws EntityNotFound {
        ContentConfig contentConfig = contentConfigRepository.getContentConfigById(id);
        if(contentConfig == null){
            throw new EntityNotFound("Content Config not found");
        }
        contentConfigRepository.deleteContentConfig(id);
    }


}
