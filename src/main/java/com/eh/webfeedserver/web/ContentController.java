package com.eh.webfeedserver.web;

import com.eh.webfeedserver.content.config.ContentConfig;
import com.eh.webfeedserver.content.config.ContentConfigRepository;
import com.eh.webfeedserver.errorhandling.EntityNotFound;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by evanharris on 3/29/15.
 */
@Controller
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private ContentConfigRepository contentConfigRepository;


    @RequestMapping(value = "/**", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response, HttpServletRequest request) throws EntityNotFound {
        try {

            String fileName = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
            fileName = fileName.substring(9);
            ContentConfig contentConfig = contentConfigRepository.getContentConfigByUrlPath(fileName);
            if(contentConfig == null ){
                    throw new EntityNotFound("no content config found for " + fileName);
            }else if(!contentConfig.isInFeed()){
                throw new EntityNotFound("content has been removed from feed for  " + fileName);
            }
            InputStream is = new FileInputStream(contentConfig.getCurrentContent().getFile());
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }

    }
}
