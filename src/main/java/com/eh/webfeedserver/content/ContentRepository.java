package com.eh.webfeedserver.content;

import com.eh.webfeedserver.errorhandling.ContentException;

import java.io.File;
import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
public interface ContentRepository {

    File getFileFromPath(String path);

    List<Content> getContentInDirectory(String dir) throws ContentException;


}
