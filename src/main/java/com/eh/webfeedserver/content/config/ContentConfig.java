package com.eh.webfeedserver.content.config;

import com.eh.webfeedserver.content.Content;
import com.eh.webfeedserver.content.ContentRepository;
import com.eh.webfeedserver.errorhandling.ContentException;
import com.eh.webfeedserver.feed.FeedItem;
import com.eh.webfeedserver.feed.FeedItemRepository;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
public class ContentConfig {

    private Integer id;
    private String feedType;
    private String contentLocation;
    private String urlPath;
    private Integer currentContentVersion;
    private Integer totalVersions;
    private Boolean isInFeed;
    private Boolean isStatic;

    private String baseUrl;

    private List<Content> contentVersions;

    private FeedItemRepository feedItemRepository;

    private String contentUrl;

    public ContentConfig(String baseUrl, ContentRepository contentRepository, FeedItemRepository feedItemRepository, Integer id, String feedType, String contentLocation,
                         Boolean isInFeed, String urlPath) throws ContentException {



        this.feedItemRepository = feedItemRepository;
        this.id = id;
        this.feedType = feedType;
        this.contentLocation = contentLocation;
        this.baseUrl = baseUrl;
        this.urlPath = urlPath;
        List<Content> contents = contentRepository.getContentInDirectory(contentLocation);


        if(contents.size() < 1){
            throw new ContentException("no files for content directory " + contentLocation);
        }

        if(StringUtils.isEmpty(this.urlPath)){
            this.urlPath = this.contentLocation;
        }

        this.totalVersions = contents.size();
        this.contentVersions = contents;
        this.isInFeed = isInFeed;
        this.currentContentVersion = 1;
        this.isStatic = false;
        if(this.totalVersions < 2){
            this.isStatic = true;
        }

        this.contentUrl = this.baseUrl + "/content/" + this.urlPath;
        syncFeedItem();
    }

    private void syncFeedItem(){

        FeedItem item = this.feedItemRepository.getFeedItem(this.contentUrl);

        if(!isStatic && currentContentVersion > totalVersions){
            this.isInFeed = false;
            if(item != null){
                this.feedItemRepository.deleteFeedItemByUrl(this.contentUrl);
            }
            return;
        }

        Boolean shouldInsert = false;
        if(item == null){
            item = new FeedItem();
            shouldInsert = true;
        }

        item.setTitle(this.contentLocation);
        item.setUrl(this.contentUrl);
        item.setUpdated(new Date());
        item.setSummary("Updated " + item.getUpdated().toString());
        item.setFeedType(this.getFeedType());

        if(shouldInsert){
            this.feedItemRepository.insertFeedItem(item);
        }
    }


    public Integer getId() {
        return id;
    }

    public String getFeedType() {
        return feedType;
    }

    public String getContentLocation() {
        return contentLocation;
    }

    public Integer getCurrentContentVersion() {
        return currentContentVersion;
    }

    public Content getCurrentContent(){

        if(this.isStatic){
            return contentVersions.get(this.currentContentVersion - 1);
        }else if(this.currentContentVersion <= this.totalVersions){
            incrementCurrentVersion();
            syncFeedItem();
            return contentVersions.get(this.currentContentVersion - 2);
        }else{
            return null;
        }


    }

    public void cleanupBeforeDelete(){
        this.feedItemRepository.deleteFeedItemByUrl(this.contentUrl);

    }

    private void incrementCurrentVersion(){
        this.currentContentVersion++;
    }

    public Integer getTotalVersions() {
        return totalVersions;
    }

    public Boolean isInFeed() {
        return isInFeed;
    }

    public Boolean isStatic(){
        return isStatic;
    }

    public String getUrlPath() {
        return urlPath;
    }
}
