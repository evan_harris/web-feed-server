package com.eh.webfeedserver.content.config;

import com.eh.webfeedserver.content.ContentRepository;
import com.eh.webfeedserver.errorhandling.ContentException;
import com.eh.webfeedserver.feed.FeedItemRepository;
import com.google.common.collect.ImmutableList;

import java.util.*;

/**
 * Created by evanharris on 3/29/15.
 */
public class ContentConfigRepositoryImpl implements ContentConfigRepository {


    private Map<Integer, ContentConfig> feedConfigMap = new HashMap<Integer, ContentConfig>();
    private Map<String, ContentConfig> contentLocationMap = new HashMap<String, ContentConfig>();
    private Map<String, ContentConfig> urlPathMap = new HashMap<>();
    private List<ContentConfig> contentConfigList = new LinkedList<ContentConfig>();
    private Random random = new Random();

    private ContentRepository contentRepository;
    private FeedItemRepository feedItemRepository;


    public ContentConfigRepositoryImpl(ContentRepository contentRepository, FeedItemRepository feedItemRepository){
        this.contentRepository = contentRepository;
        this.feedItemRepository = feedItemRepository;

    }

    @Override
    public ContentConfig insertContentConfig(String feedType, String contentLocation,
                                             Boolean isInFeed, String basePath, String urlPath) throws ContentException {

        ContentConfig config = getContentConfigByContentPath(contentLocation);
        if(config != null){
            return config;
        }

        Boolean isInFeedDefaulted = isInFeed;

        if(isInFeed == null){
            isInFeedDefaulted = true;
        }

        ContentConfig newconfig = new ContentConfig(basePath, contentRepository, feedItemRepository, random.nextInt(), feedType, contentLocation,
               isInFeedDefaulted, urlPath);

        contentConfigList.add(newconfig);
        feedConfigMap.put(newconfig.getId(), newconfig);
        contentLocationMap.put(newconfig.getContentLocation(), newconfig);
        urlPathMap.put(newconfig.getUrlPath(), newconfig);
        return newconfig;
    }

    @Override
    public List<ContentConfig> getAllContentConfigs() {
        return ImmutableList.copyOf(contentConfigList);
    }

    @Override
    public ContentConfig getContentConfigById(Integer id) {
        return feedConfigMap.get(id);
    }

    @Override
    public ContentConfig getContentConfigByContentPath(String contentPath) {

            return contentLocationMap.get(contentPath);
    }

    @Override
    public void deleteContentConfig(Integer id) {
        ContentConfig contentConfig = this.getContentConfigById(id);
        if(contentConfig != null){
            contentConfig.cleanupBeforeDelete();
            contentConfigList.remove(contentConfig);
            feedConfigMap.remove(contentConfig.getId());
            contentLocationMap.remove(contentConfig.getContentLocation());
            urlPathMap.remove(contentConfig.getUrlPath());
        }
    }


    @Override
    public ContentConfig getContentConfigByUrlPath(String urlPath){
        return urlPathMap.get(urlPath);
    }


}
