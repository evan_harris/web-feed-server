package com.eh.webfeedserver.content.config;

import com.eh.webfeedserver.errorhandling.ContentException;

import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
public interface ContentConfigRepository {

    ContentConfig insertContentConfig(String feedType, String contentLocation,
                                     Boolean isInFeed, String basePath, String urlPath) throws ContentException;

    List<ContentConfig> getAllContentConfigs();

    ContentConfig getContentConfigById(Integer id);

    ContentConfig getContentConfigByContentPath(String contentPath);

    ContentConfig getContentConfigByUrlPath(String urlPath);

    void deleteContentConfig(Integer id);


}
