package com.eh.webfeedserver.content;

import com.eh.webfeedserver.errorhandling.ContentException;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
public class ContentRepositoryImpl implements ContentRepository {

    private String baseDirPath;

    public ContentRepositoryImpl(String baseDirPath) throws ContentException {

        File basedir = new File(baseDirPath);
        if(!basedir.exists()){
            throw new ContentException("Base content path: " + basedir.getAbsolutePath() + " does not exit");
        } else if(!basedir.isDirectory()){
            throw new ContentException("Base content path: " + basedir.getAbsolutePath() + " is not a directory");
        }

        this.baseDirPath = baseDirPath;

    }

    @Override
    public File getFileFromPath(String path) {
        File file = new File(baseDirPath + '/' + path);

        if(file.exists()){
            return file;
        }
        return null;
    }

    @Override
    public List<Content> getContentInDirectory(String dir) throws ContentException {
        File directory = new File(baseDirPath + '/' + dir);

        if(directory.exists() && directory.isDirectory()){

            List<Content> contentList = new LinkedList<>();
            for(File file : directory.listFiles()){
                contentList.add(new Content(file));
            }
            Collections.sort(contentList);
            return ImmutableList.copyOf(contentList);
        }else{
            throw new ContentException("directory does not exist " + directory.getAbsolutePath());
        }
    }


}
