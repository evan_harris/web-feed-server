package com.eh.webfeedserver.content;

import com.eh.webfeedserver.errorhandling.ContentException;

import java.io.File;

/**
 * Created by evanharris on 3/29/15.
 */
public class Content implements Comparable{

    private File file;

    public Content(File contentFile) throws ContentException {
        file = contentFile;

        if(!contentFile.exists() || contentFile.isDirectory()){
            throw new ContentException("cannot create content from nothing or directory " + contentFile.getAbsolutePath());
        }


    }

    public File getFile(){
        return file;
    }


    @Override
    public int compareTo(Object aThat) {
        final int BEFORE = -1;
        final int EQUAL = 0;

        //this optimization is usually worthwhile, and can
        //always be added
        if (this == aThat) return EQUAL;

        if(!(aThat instanceof Content)){
            return BEFORE;
        }
        Content that = (Content)aThat;
        return this.getFile().compareTo(that.getFile());
    }

}
