package com.eh.webfeedserver.errorhandling;

/**
 * Created by evanharris on 3/29/15.
 */
public class ContentException extends Exception {

    public ContentException(String message){
        super(message);

    }
}
