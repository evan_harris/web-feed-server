package com.eh.webfeedserver.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by evanharris on 3/29/15.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFound extends Exception {

    public EntityNotFound(String message){
        super(message);
    }
}
