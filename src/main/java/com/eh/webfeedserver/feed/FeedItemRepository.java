package com.eh.webfeedserver.feed;

import java.util.List;

/**
 * Created by evanharris on 3/29/15.
 */
public interface FeedItemRepository {

    FeedItem getFeedItem(String url);

    List<FeedItem> getAllFeedItems();

    List<FeedItem> getFeedItemsByFeedType(String feedType);

    void insertFeedItem(FeedItem feedItem);

    void deleteFeedItemByUrl(String url);


}
