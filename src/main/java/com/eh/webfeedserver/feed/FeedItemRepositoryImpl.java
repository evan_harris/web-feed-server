package com.eh.webfeedserver.feed;

import com.google.common.collect.ImmutableList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by evanharris on 3/29/15.
 */
public class FeedItemRepositoryImpl implements FeedItemRepository {

    Map<String, FeedItem> feedItemMap = new HashMap<String, FeedItem>();
    List<FeedItem> feedItemList = new LinkedList<FeedItem>();
    Map<String, List<FeedItem>> feedItemsByType = new HashMap<String, List<FeedItem>>();

    @Override
    public FeedItem getFeedItem(String url) {
        return feedItemMap.get(url);
    }

    @Override
    public List<FeedItem> getAllFeedItems() {
        return ImmutableList.copyOf(feedItemList);
    }

    @Override
    public List<FeedItem> getFeedItemsByFeedType(String feedType) {
        return feedItemsByType.get(feedType);
    }

    @Override
    public void insertFeedItem(FeedItem feedItem) {
        feedItemList.add(feedItem);
        feedItemMap.put(feedItem.getUrl(), feedItem);

        String feedType = feedItem.getFeedType();
        if(feedType != null){
            if(!feedItemsByType.containsKey(feedType)){
                feedItemsByType.put(feedType, new LinkedList<FeedItem>());
            }
            feedItemsByType.get(feedType).add(feedItem);
        }
    }

    @Override
    public void deleteFeedItemByUrl(String url) {
        FeedItem feedItem = feedItemMap.get(url);
        if(feedItem != null){
            feedItemList.remove(feedItem);
            feedItemMap.remove(url);
        }
    }
}
